# Hearsay #
A HipChat Connect add-on based on
[ac-koa-hipchat](https://bitbucket.org/atlassianlabs/ac-koa-hipchat) that
listeners for configurable regexes in a room and replies with one of N
possible response messages.

![HipChat - Integrations.png](https://bitbucket.org/repo/6ABxk6/images/801977502-HipChat%20-%20Integrations.png)

Whenever someone says something that matches the regex in the listener:
![HipChat - Search Results-1.png](https://bitbucket.org/repo/6ABxk6/images/1134422970-HipChat%20-%20Search%20Results-1.png)

### [Install Me](https://hipchat.com/addons/install?url=https%3A%2F%2Fac-koa-hipchat-hearsay.herokuapp.com%2Faddon%2Fcapabilities) ###